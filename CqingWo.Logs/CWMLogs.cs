﻿using System;
using System.IO;

namespace CqingWo.Logs
{
    /// <summary>
    /// 青沃日志测试
    /// </summary>
    public class CWMLogs
    {
        /// <summary>
        /// 锁定对象
        /// </summary>
        private static object _locker = new object();//锁对象

        /// <summary>
        /// 日志输出
        /// </summary>
        /// <param name="error">日志错误</param>
        public static void WriteLog(string error)
        {
            CWMLogs.WriteLog(error, null);
        }

        /// <summary>
        /// 日志输出
        /// </summary>
        /// <param name="error">日志错误</param>
        /// <param name="ex">异常</param>
        public static void WriteLog(string error, Exception ex)
        {
            try
            {

                lock (_locker)
                {
                    string text = AppDomain.CurrentDomain.BaseDirectory + "\\logs";

                    string path = text + "\\log_" + DateTime.Now.ToString("yyyy-MM-dd") + ".log";

                    FileInfo fileInfo = new FileInfo(path);
                    if (!fileInfo.Directory.Exists)
                    {
                        fileInfo.Directory.Create();
                    }
                    if (!fileInfo.Exists)
                    {
                        fileInfo.Create().Close();
                    }
                    else if (fileInfo.Length > 2048 * 1000)
                    {
                        fileInfo.Delete();
                    }
                    if (!Directory.Exists(text))
                    {
                        Directory.CreateDirectory(text);
                    }
                    error = DateTime.Now.ToShortTimeString() + " " + error + Environment.NewLine;
                    if (ex != null)
                    {
                        error = error + ex.ToString() + Environment.NewLine;
                    }

                    File.AppendAllText(path, error);
                }
            }
            catch (Exception)
            {

                
            }

        }
    }
}
